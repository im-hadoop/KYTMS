package com.kytms.rule.core;

import javax.script.*;
import java.util.HashMap;
import java.util.Map;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2019-07-22
 */
public abstract class RuleUtils {
    public static final Map<String,Object>  ruleCache = new HashMap<String, Object>();
    public static Object runRule(String ruleName,String ages){
        return null;
    }
    static Compilable engine;  //获取groovy编译类
    static {
        ScriptEngineManager manager = new ScriptEngineManager();
        engine = (Compilable) manager.getEngineByName("groovy");
    }
    /**
     * 规则上线
     * @param ruleName
     * @param rule
     */
    public static void ruleOilne(String ruleName,String rule) throws ScriptException, NoSuchMethodException {




        String ruleTempler ="def  run(Object obj){"+ rule +
                        " return '1234';"+
                        "    }";
        ScriptEngineManager factory = new ScriptEngineManager();
        //每次生成一个engine实例
        ScriptEngine engine = factory.getEngineByName("groovy");
        System.out.println(engine.toString());
        assert engine != null;
        //javax.script.Bindings
        //如果script文本来自文件,请首先获取文件内容
        engine.eval(ruleTempler);
        String o= (String) ((Invocable)engine).invokeFunction("run",null);
        System.out.println(o);

    }

    /**
     * 规则下线
     * @param ruleName
     */
    public static void ruleUnOilne(String ruleName){
        ruleCache.remove(ruleName);
    }
}
