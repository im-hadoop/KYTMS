package com.kytms.orderback.dao.Impl;

import com.kytms.core.dao.impl.BaseDaoImpl;
import com.kytms.core.entity.OrderBackImages;
import com.kytms.orderback.dao.OrderBackImagesDao;
import org.springframework.stereotype.Repository;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * @author 臧英明
 * @create 2018-01-19
 */
@Repository(value = "OrderBackImagesDao")
public class OrderBackImagesDaoImpl extends BaseDaoImpl<OrderBackImages> implements OrderBackImagesDao<OrderBackImages> {
}
